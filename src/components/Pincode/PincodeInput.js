import React from "react";
import styled from "styled-components";
import { defaultTheme } from "../../utils/theme";
import PropTypes from "prop-types";
import { StyleInjector } from "../StyleInjector";

export const PincodeInput = ({ id, defaultVal, setRef, onChange }) => {
  const [value, setValue] = React.useState(null);

  return (
    <PinInput
      ref={(r) => setRef && setRef(r)}
      maxLength={1}
      id={id}
      defaultValue={defaultVal}
      value={value}
      onChange={(e) => {
        onChange && onChange();
        setValue(e.nativeEvent.data);
      }}
    />
  );
};

const PinInput = styled.input`

  display: flex;
  width: 47px;
  height: 55px;
  1px solid rgba(0, 0, 0, 0.42); 
  text-align: center !important;

  ${defaultTheme.mobileFonts.h2Semibold};
  &:hover {
    border: 2px solid #1F5D57;
  };
  &:active {
    border: 2px solid #1F5D57;
  }

`;

PincodeInput.propTypes = {
  /**
   * ID
   */
   id: PropTypes.string,
  /**
   * Valor predeterminado, en caso de tenerlo
   */
  defaultVal: PropTypes.string,
  /**
   * Función para recoger la referencia del input, para recoger valores o seleccionar estados
   */
  setRef: PropTypes.func,

  /**
   * Función que salta al cambiar valor
   */
   onChange: PropTypes.func,
};

PincodeInput.defaultProps = {
  defaultVal: null,
  setRef: null,
  onChange: null,
  id: null,
};
