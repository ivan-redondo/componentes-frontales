import React from "react";
import { PincodeInput } from ".";
import PropTypes from "prop-types";
import styled from "styled-components";

export const PincodeGroup = ({ quantity, sendFinalValue }) => {
  let arr = [...Array(quantity)];
  let finalVal = '';

  const onChange = (index) => {
    if (index < arr.length - 1) {
      let currentInput = document.getElementById("pin_" + index);
      let nextInput = document.getElementById("pin_" + (index + 1));

      if (nextInput) {
        if(currentInput.value?.length > 0){
          nextInput.focus();
        }
      }
    }
    getFinalVal();
  };
  
  const getFinalVal = () => {
    finalVal = '';
    for(let i = 0; i < arr.length; i++){
      let input = document.getElementById("pin_" + (i));
      let val = input.value || ''
      finalVal = finalVal + val;
    }

    sendFinalValue && sendFinalValue(finalVal)
  }

  return (
    <PinWrapper>
      {arr.map((item, index) => {
        return (
          <PincodeInput
            id={"pin_" + index}
            key={index}
            onChange={() => {
              onChange(index);
            }}
          />
        );
      })}
    </PinWrapper>
  );
};

const PinWrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

PincodeGroup.propTypes = {
  /**
   * Cantidad de inputs
   */
  quantity: PropTypes.number,
  /**
   * Envía al padre el resultado del pincode introducido
   */
  sendFinalValue: PropTypes.func
};

PincodeGroup.defaultProps = {
  quantity: 1,
  sendFinalValue: undefined,
};
