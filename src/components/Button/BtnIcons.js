import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

export const BtnIcons = ({ mobile, onPress, Icon }) => {
    const onButtonPress = () => {
        onPress && onPress();
    }

    return (
        <ArrowContainer
            onClick={() => {
                onButtonPress();
            }}
            mobile={mobile}
        >
            <Icon />
        </ArrowContainer>
    )

}

const ArrowContainer = styled.button`
    display:flex;
    align-items: center;
    justify-content: center;
    border-style: none;
    text-align: center;
    background: transparent;
    height: 40px;
    width: 40px;
    cursor: pointer;

    &:hover {
        background-color: #F5F5F5;
        border-radius: 6px;
    }

    &:active {
        background-color: #D6D6D6;
    }

    ${({ mobile }) =>
        mobile && `
            margin-left: 35px;
    `};
`;


BtnIcons.propTypes = {
    /**
     * Evento al hacer click
     */
    onPress: PropTypes.func,
    /**
     * ¿Se está mostrando en móvil?
     */
    mobile: PropTypes.bool,
    /**
    * Icono del botón
    */
    Icon: PropTypes.func,
};

BtnIcons.defaultProps = {
    onPress: undefined,
    mobile: false,
    Icon: undefined,
}