import React from 'react';
import styled from 'styled-components';
import { defaultTheme } from '../../utils/theme';
import PropTypes from 'prop-types';
import { Text } from '../Text/Text';

export const BtnText = ({ mobile, title, subtitle, Icon, onPress, fontTilte, fontSubtitle }) => {
  const onButtonPress = () => {
    onPress && onPress();
  }

  return (
    <Container 
      mobile={mobile} 
      onClick={() => onButtonPress()}
    >
      <IconDiv>
        <Icon />
      </IconDiv>
      <TextsContainer>
        <TitleText 
          className="title" 
          cursor="pointer" 
          padBot="4px" 
          mobile={mobile} 
          theme={fontTilte}
        >{title}</TitleText>
        <SubtitleText 
          cursor="pointer" 
          mobile={mobile} 
          theme={fontSubtitle}
        >{subtitle}</SubtitleText>
      </TextsContainer>
    </Container>
  )
}

const Container = styled.button`
  display: flex;
  flex-direction: row;
  border-style: none;
  background: transparent; 
  padding-bottom: 32px;
  padding:${({ mobile }) => mobile ? '16px 24px' : '0px 0px 32px 0px'};
  margin-left: ${({ mobile }) => mobile && '-32px'};
  &:hover .title {
    text-decoration: ${({ mobile }) => !mobile && 'underline'};
  };
  &:hover {
    background-color:${({ mobile }) => mobile && '#D6D6D6'};
  };
  &:active {
    background-color:${({ mobile }) => mobile && '#D6D6D6'};
  };
`;

const TextsContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const IconDiv = styled.div`
  width:18px;
  height:18px;
  margin-right: 17px;
  margin-top: 2px;
`;


const TitleText = styled(Text)`

`;

const SubtitleText = styled(Text)`
  padding-bottom: ${(props) => (props.padBot ? props.padBot : "0px")};
  margin-left: ${(props) => (props.mLeft ? props.mLeft : "0px")};
  margin-right: ${(props) => (props.mRight ? props.mRight : "0px")};
  cursor: pointer;
  ${({ mobile, theme }) =>
    mobile
      ? `
  ${defaultTheme.mobileFonts[theme]}
  `
      : `
  ${defaultTheme.desktopFonts[theme]}
  `}

`;

BtnText.propTypes = {
  /**
   * Titulo
   */
  title: PropTypes.string.isRequired,
  /**
   * Subtitle
   */
  subtitle: PropTypes.string.isRequired,
  /**
   * Icono
   */
  Icon: PropTypes.func,
  /**
   * Evento al hacer click
   */
  onPress: PropTypes.func,
  /**
    * ¿Es versión móvil?
    */
  mobile: PropTypes.bool,
};

BtnText.defaultProps = {
  title: '900 52 52 74',
  subtitle: 'Horario de lunes a viernes de 9h a 18:30h.',
  Icon: undefined,
  onPress: undefined,
  mobile: false
}
