import styled from "styled-components";
import PropTypes from "prop-types";
import { Text } from '../Text/Text';

export const BtnModal = ({ onPress, disabled, text, mobile, IconLeft, IconRight, font }) => {
    const onButtonPress = () => {
        onPress && onPress();
    }

    return (
        <Button
            onClick={() => {
                 onButtonPress();
            }}
            disabled={disabled}
            mobile={mobile}
        >
            {
                IconLeft &&
                    <IconLeft />
            }
            <Text2 
                theme={font} 
                IconLeft={IconLeft} 
                IconRight={IconRight}
            >{text}</Text2>
            {
                IconRight &&
                    <IconRight />
            }
        </Button>
    );
}

const Button = styled.button`
  display: flex;
  flex-direction: row;
  border-style: none;
  justify-content: center;
  align-items: center;
  border-radius: 0px 0px 6px 6px;
  background-color: #fff;
  height: 56px;
  font-weight: 600;
  line-height: 21,6px;
  padding: 17px 16px;
  border-top: 1px solid #D6D6D6;
  cursor: pointer;

  &:hover {
    background-color: #F5F5F5;
  }

  &:active {
    background-color: #D6D6D6;
  }
`;

const Text2 = styled(Text)`
    cursor: pointer;
    color: #25807E;

    ${({ IconLeft }) =>
        IconLeft &&
            `
                margin-left: 15px;
            `
    }

    ${({ IconRight }) =>
        IconRight &&
            `
                margin-right: 15px;
            `
    }

`;

BtnModal.propTypes = {
    /**
     * Evento al hacer click
     */
    onPress: PropTypes.func,
    /**
     * Botón desabilitado
     */
    disabled: PropTypes.bool,
    /**
     * Texto del botón
     */
    text: PropTypes.string.isRequired,
    /**
     * ¿Es versión móvil?
     */
    mobile: PropTypes.bool,
    /**
     * Icono del botón
     */
    IconLeft: PropTypes.func,
        /**
     * Icono del botón
     */
    IconRight: PropTypes.func,
};

BtnModal.defaultProps = {
    onPress: undefined,
    mobile: false,
    text: "Introduce texto",
    disabled: true,
    IconLeft: undefined,
    IconRight: undefined,
};