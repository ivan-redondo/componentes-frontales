import React from "react";
import styled, { keyframes } from "styled-components";
import { defaultTheme } from "../../utils/theme";
import PropTypes from "prop-types";
import Spinner from "../../assets/images/svg/Spinner";
import TickIcon from "../../assets/images/svg/TickIcon";
import { Text } from "../Text/Text";

export const BtnDefault = ({ onPress, disabled, loading, success, text, mobile, colorDefault, colorSecondary, IconLeft, IconRight, font }) => {
  const onButtonPress = () => {
    onPress && onPress();
  };

  return (
    <Button
      onClick={() => {
        onButtonPress();
      }}
      disabled={disabled}
      success={success}
      mobile={mobile}
      success={success}
      colorDefault={colorDefault}
      colorSecondary={colorSecondary}
    >
      {loading ? (
        <AnimatedSpinner>
          <Spinner color="#FFF" />
        </AnimatedSpinner>
      ) : (
        success && <TickIcon color="#fff"/>
      )}

      {!success && IconLeft && !loading && <IconLeft color={"#fff"} />}
      {!success && !loading && <Text2 disabled={disabled} theme={font} IconLeft={IconLeft} IconRight={IconRight}>{text}</Text2>}
      {!success && IconRight && !loading && <IconRight color={"#fff"} />}
    </Button>
  );
};

const Button = styled.button`
  display: flex;
  flex-direction: row;
  border-style: none;
  justify-content: center;
  align-items: center;
  width: 100%;
  min-width: ${({ mobile }) => (mobile ? "100%" : "auto")};
  min-height: 56px;
  padding: ${({ mobile }) => (mobile ? "11px 16px 11px 16px" : "17px 24px 17px 24px")};
  background: ${({ success, disabled, colorDefault, colorSecondary }) => (success ? defaultTheme.spinner : disabled ? colorSecondary : colorDefault)};
  cursor: ${(props) => (props.disabled ? "default" : "pointer")};
  border-radius: 6px;
  min-width: 134px;
`;

const Text2 = styled(Text)`
  ${({ mobile }) => (mobile ? defaultTheme.mobileFonts.button1Semibold : defaultTheme.desktopFonts.button1Semibold)};
  color: #ffffff;
  cursor: ${(props) => (props.disabled ? "default" : "pointer")};
  ${({ IconLeft }) =>
    IconLeft &&
    `
      margin-left: 15px;
    `}

  ${({ IconRight }) =>
    IconRight &&
    `
      margin-right: 15px;
    `}
`;

const AnimatedSpinner = styled.div`
  width: 22px;

  svg {
    height: 18px;
    animation: 1s
      ${keyframes({
        from: { transform: "rotate(0deg)" },
        to: { transform: "rotate(360deg)" },
      })}
      linear;
    animation-iteration-count: infinite;
  }
`;

BtnDefault.propTypes = {
  /**
   * Evento al hacer click
   */
  onPress: PropTypes.func,
  /**
   * Botón desabilitado
   */
  disabled: PropTypes.bool,
  /**
   * ¿Se muestra el spinner?
   */
  loading: PropTypes.bool,
  /**
   * ¿Se a realizado el cambio?
   */
  success: PropTypes.bool,
  /**
   * Texto del botón
   */
  text: PropTypes.string.isRequired,
  /**
   * ¿Es versión móvil?
   */
  mobile: PropTypes.bool,
  /**
   * Color del fondo del botón
   */
  color: PropTypes.string,
  /**
   * Icono del botón
   */
  IconLeft: PropTypes.func,
  /**
   * Icono del botón
   */
  IconRight: PropTypes.func,
};

BtnDefault.defaultProps = {
  onPress: undefined,
  mobile: false,
  text: "Guardar cambios",
  disabled: true,
  loading: false,
  success: false,
  IconLeft: undefined,
  IconRight: undefined,
};
