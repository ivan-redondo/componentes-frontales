import React from "react";
import { Text } from "../Text/Text";
import styled from "styled-components";
import PropTypes from "prop-types";
import ArrowB from "../../assets/images/svg/ArrowB";

export const Dropdown = ({
  label1,
  label2,
  children,
  mobile,
  opened,
  onDropdownClick,
  AuxiliarIcon,
  onAuxiliarIconClick,
}) => {
  return (
    <ContainerDropDown
      onClick={() => {
        onDropdownClick && onDropdownClick();
      }}
    >
      <ContainerTitleDropDown>
        <SpaceContainer>
          <Text mobile={mobile} theme="body1Semibold">
            {label1}
          </Text>
          {label2 && (
            <Text mobile={mobile} theme="body1Medium" mRight={20}>
              {label2}
            </Text>
          )}
        </SpaceContainer>
        {AuxiliarIcon && (
          <ImgContainer
            onClick={() => {
              onAuxiliarIconClick && onAuxiliarIconClick();
            }}
          >
            <AuxiliarIcon />
          </ImgContainer>
        )}
        {opened ? (
          <RotatedDiv>
            <ArrowB />
          </RotatedDiv>
        ) : (
          <ArrowB />
        )}
      </ContainerTitleDropDown>
      <ContainDropVisible>{opened && children && children}</ContainDropVisible>
    </ContainerDropDown>
  );
};

Dropdown.propTypes = {
  /**
   * Texto principal (Obligatorio)
   */
  label1: PropTypes.string.isRequired,
  /**
   * Texto secundario (Opcional)
   */
  label2: PropTypes.string,
  /**
   * Contenido a renderizar dentro del desplegable
   */
  children: PropTypes.object,
  /**
   * ¿Estamos en un dispositivo móvil?
   */
  mobile: PropTypes.bool,
  /**
   * Parámetro que despliega el contenido (Obligatorio)
   */
  opened: PropTypes.bool.isRequired,
  /**
   * Acción que ocurre al pulsar el desplegable
   */
  onDropdownClick: PropTypes.func,
  /**
   * Icono Auxiliar a mostrar en el Desplegable
   */
  AuxiliarIcon: PropTypes.func,
  /**
   * Función que será ejecutada al pulsar el icono auxiliar
   */
  onAuxiliarIconClick: PropTypes.func,
};

Dropdown.defaultProps = {
  onDropdownClick: null,
  children: null,
  label1: "Texto",
  label2: "",
  opened: false,
  AuxiliarIcon: undefined,
  onAuxiliarIconClick: null,
  mobile: false,
};

const ContainerDropDown = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin-top: 18px;
  cursor: pointer;
`;

const ContainerTitleDropDown = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
`;

const SpaceContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
  width: 100%;
`;

const ImgContainer = styled.div`
  cursor: pointer;
`;

const Img = styled.img`
  cursor: pointer;
`;

const RotatedDiv = styled.div`
  transform: rotate(180deg);
`;

const ContainDropVisible = styled.div`
  padding-bottom: 18px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.09);
`;
