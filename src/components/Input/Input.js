import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { defaultTheme } from "../../utils/theme";
import PropTypes from "prop-types";
import Cross from "../../assets/images/svg/Cross";

export const Input = ({ required, placeholder, value, label, type, onChange, onIntroPress, width, disabled, error, errorText }) => {
  const [valueInput, setValueInput] = useState(value);
  const [focus, setFocus] = useState(false);
  const [mobile, setMobile] = useState(window.innerWidth < 768 ? true : false);

  useEffect(() => {
    window.addEventListener("resize", () => setMobile(window.innerWidth < 768 ? true : false));
  }, []);

  useEffect(() => {
    setValueInput(value);
  }, [value]);

  const eventChange = (e) => {
    setValueInput(e.target.value);
    onChange && onChange(e.target.value);
  };

  const eventKeyPress = (e) => {
    const enterKey = 13;
    if (e.which === enterKey) {
      onIntroPress && onIntroPress();
    }
  };

  const reset = () => {
    setValueInput("");
  };

  return (
    <Container width={width}>
      <InputCustom
        disabled={disabled}
        required={required}
        placeholder={placeholder}
        type={type}
        value={valueInput || ""}
        mobile={mobile}
        error={error}
        onChange={(e) => eventChange(e)}
        onKeyPress={(e) => eventKeyPress(e)}
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
      />
      {valueInput && valueInput.length > 0 && <Cross id="cross" onClick={() => reset()} />}
      <Label mobile={mobile} error={error} disabled={disabled} top={(valueInput && valueInput.length > 0) || focus}>
        {label}
      </Label>
      {error && <LabelError mobile={mobile}>{errorText}</LabelError>}
    </Container>
  );
};

const Container = styled.div`
  width: ${(props) => (props.width ? props.width : "100%")};
  position: relative;

  && > svg {
    position: absolute;
    right: 22px;
    top: 23px;
    cursor: pointer;
    pointer-events: all;
  }
`;

const InputCustom = styled.input`
  width: 100%;
  height: 56px;
  background: #ffffff;
  border: ${(props) => (props.error ? "1px solid #D80E36" : "1px solid #ebebeb")};
  box-sizing: border-box;
  border-radius: 6px;
  color: black;
  padding-top: 15px;
  padding-left: 12px;
  padding-right: 50px;
  ${({ mobile }) =>
    mobile
      ? `
    ${defaultTheme.mobileFonts.body1Regular}
  `
      : !mobile &&
        `
    ${defaultTheme.desktopFonts.body1Regular}
  `}

  &:focus {
    border: ${(props) => (props.error ? "2px solid #D80E36" : "2px solid #000000")};
  }

  &::-webkit-input-placeholder {
    color: transparent;
  }
  &:-moz-placeholder {
    color: transparent;
  }
  &::-moz-placeholder {
    color: transparent;
  }
  &:-ms-input-placeholder {
    color: transparent;
  }
  &:focus::-webkit-input-placeholder {
    color: rgba(0, 0, 0, 0.42);
    ${defaultTheme.desktopFonts.body1Regular};
  }
  &:focus:-moz-placeholder {
    color: rgba(0, 0, 0, 0.42);
    ${defaultTheme.desktopFonts.body1Regular};
  }
  &:focus::-moz-placeholder {
    color: rgba(0, 0, 0, 0.42);
    ${defaultTheme.desktopFonts.body1Regular};
  }
  &:focus:-ms-input-placeholder {
    color: rgba(0, 0, 0, 0.42);
    ${defaultTheme.desktopFonts.body1Regular};
  }
`;

const Label = styled.span`
  position: absolute;
  color: ${(props) => (props.disabled ? "rgba(0, 0, 0, 0.28)" : props.error ? "#d80e36;" : "rgba(0, 0, 0, 0.42)")};
  pointer-events: none;
  left: 13px;
  top: ${(props) => (!props.top ? "20px" : "6px")};
  transition: 0.1s ease all;
  ${({ mobile }) =>
    mobile
      ? `
    ${defaultTheme.mobileFonts.body1Regular}
  `
      : !mobile &&
        `
    ${defaultTheme.desktopFonts.body1Regular}
  `}
  ${({ top, mobile }) =>
    top &&
    mobile &&
    `
    font-size: 13px;
  `}
  ${({ top, mobile }) =>
    top &&
    !mobile &&
    `
    font-size: 12px;
  `}
`;

const LabelError = styled.span`
  ${({ mobile }) =>
    mobile
      ? `
    ${defaultTheme.mobileFonts.overline1Regular}
  `
      : !mobile &&
        `
    ${defaultTheme.desktopFonts.overline1Regular}
  `}
  color: #d80e36;
  text-transform: none;
`;

Input.prototypes = {
  required: PropTypes.bool,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  onChange: PropTypes.func,
  onIntroPress: PropTypes.func,
  width: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.bool,
  errorText: PropTypes.string,
};

Input.defaultProps = {
  required: false,
  placeholder: "",
  value: "",
  label: "prueba",
  type: "",
  onChange: PropTypes.func,
  onIntroPress: PropTypes.func,
  width: "100px",
  disabled: false,
  error: false,
  errorText: "",
};
