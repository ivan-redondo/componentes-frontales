import React, { useEffect, useRef } from "react";
import styled, { css, keyframes } from "styled-components";
import PropTypes from "prop-types";

export const SwitchItem = ({ value, onClick }) => {
  const [firstClick, setFirstClick] = React.useState(false);

  const usePrevious = (value) => {
    const ref = useRef();
    useEffect(() => {
      ref.current = value;
    });
    return ref.current;
  }
  let prevValue = usePrevious(value)
  
  useEffect(()=>{

  //console.log("value:", value, ", prev:", prevValue)

    if( typeof(prevValue) !== 'undefined' && value !== prevValue){
      setFirstClick(true);
    }
  }, [value])

  return (
    <CircleContainer
      value={value}
      firstClick={firstClick}
      onClick={() => {
        setFirstClick(true);
        onClick && onClick();
      }}
    >
      <Circle value={value} firstClick={firstClick} />
    </CircleContainer>
  );
};

SwitchItem.propTypes = {
  value: PropTypes.bool,
  /**
   * Función al clickar el switch
   */
  onClick: PropTypes.func,
};

SwitchItem.defaultProps = {
  value: false,
  onClick: undefined,
};

const CircleContainer = styled.div`
  display: flex;
  background: ${({ value }) => (value == true ? "#25807E" : "#BDBDBD")};
  width: 40px;
  height: 24px;
  border-radius: 12px;
  align-items: center;
  padding-left: 2px;
  padding-right: 2px;
  cursor: pointer;
  ${({ value, firstClick }) =>
    value == true && firstClick == true
      ? css`
          animation: 0.1s
            ${keyframes({
              from: { background: "#BDBDBD" },
              to: { background: "#25807E" },
            })}
            linear;
        `
      : css`
          animation: 0.1s
            ${keyframes({
              from: { background: "#25807E" },
              to: { background: "#BDBDBD" },
            })}
            linear;
        `};
`;

const Circle = styled.div`
  width: 20px;
  height: 20px;
  background: #ffffff;
  border-radius: 16px;
  cursor: pointer;
  margin-left: ${({ value }) => (value == true ? "20px" : "0px")};

  ${({ value, firstClick }) =>
    value == true && firstClick == true
      ? css`
          animation: 0.1s
            ${keyframes({
              from: { "margin-left": "0px" },
              to: { "margin-left": "20px" },
            })}
            linear;
        `
      : css`
          animation: 0.1s
            ${keyframes({
              from: { "margin-left": "20px" },
              to: { "margin-left": "0px" },
            })}
            linear;
        `};
`;
