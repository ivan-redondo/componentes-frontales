import React from 'react';
import styled from 'styled-components';
import {defaultTheme} from '../../utils/theme';
import PropTypes from 'prop-types';

export const Text = ({padBot, mLeft, mRight, cursor, color, opacity, mobile, theme, onClick, ...props}) => {

  return(
    <SText onClick={() => {onClick && onClick()}} padBot={padBot} mLeft={mLeft} mRight={mRight} cursor={cursor} color={color} opacity={opacity} theme={theme} mobile={mobile} {...props}>
    </SText>
  )

}

const SText = styled.span`
  padding-bottom: ${(props) => (props.padBot ? (props.padBot +'px') : "0px")};
  margin-left: ${(props) => (props.mLeft ? (props.mLeft + 'px') : "0px")};
  margin-right: ${(props) => (props.mRight ? (props.mRight + 'px') : "0px")};
  cursor: ${(props) => (props.cursor === "pointer" ? "pointer" : "default")};
  opacity: ${(props) => (props.opacity ? props.opacity : "100%")};
  color: ${(props) => props.color && props.color };
  ${({ mobile, theme }) =>
    mobile
      ? `
  ${defaultTheme.mobileFonts[theme]}
`
      : `
  ${defaultTheme.desktopFonts[theme]}
`};
`;

Text.propTypes = {
  /**
   * Padding inferior
   */
  padBot: PropTypes.number,

  /**
   * Margen izquierdo
   */
   mLeft: PropTypes.number,
  /**
   * Margen derecho
   */
  mRight: PropTypes.number,
  /**
   * Tipo de cursor
   */
  cursor: PropTypes.oneOf(['pointer', 'default']),
  /**
   * Opacidad del texto
   */
  opacity: PropTypes.number,
  /**
   * Color del texto
   */
  color: PropTypes.string,
  /**
   * ¿Se está mostrando en móvil?
   */
  mobile: PropTypes.bool,
  /**
   * Tema del texto a mostrar
   */
  theme: PropTypes.oneOf(['h1Semibold', 'h1Medium', 'h1Regular', 'h2Semibold', 'h2Medium', 'h2Regular', 'h3Semibold', 'h3Medium', 'h3Regular', 'h4Semibold', 'h4Medium', 'h4Regular', 'hButtonSemibold', 'hButtonMedium', 'hButtonRegular', 'button1Semibold', 'button1Medium', 'button1Regular', 'button2Semibold', 'button2Medium', 'button2Regular', 'body1Semibold', 'body1Medium', 'body1Regular', 'body1SemiboldSlashed', 'body1MediumSlashed', 'body1RegularSlashed', 'body2Semibold', 'body2Medium', 'body2Regular', 'body2SemiboldSlashed', 'body2MediumSlashed', 'body2RegularSlashed', 'overline1Semibold', 'overline1Medium', 'overline1Regular', 'overline2Semibold', 'overline2Medium', 'overline2Regular', 'overline3Semibold', 'overline3Medium', 'overline3Regular', 'caption1Semibold', 'caption1Medium', 'caption1Regular', 'caption2Semibold', 'caption2Medium', 'caption2Regular', 'caption3Semibold', 'caption3Medium', 'caption3Regular',]).isRequired,
  /**
   * Contenido del texto
   */
  label: PropTypes.string.isRequired,
};

Text.defaultProps = {
  padBot: 0,
  mLeft: 0,
  mRight: 0,
  cursor: 'default',
  opacity: 1,
  color: 'black',
  mobile: false,
  theme: 'h1Semibold',
  label: 'Texto Alterna',
}