import React from "react";
import styled from "styled-components";
import { Text } from "../Text/Text";
import { defaultTheme } from "../../utils/theme";
import PropTypes from "prop-types";

export const Tooltip = ({ tooltipText, mobile, position, orientation, icon, show, ...props }) => {
  return (
    <ContainerSpan
      show={show}
      position={position}
      orientation={orientation}
    >
      {icon && 
        <ContainerIcon>
          {icon}
        </ContainerIcon>
      }
      <Texto>
        { tooltipText }
      </Texto>
    </ContainerSpan>
  );
};

const ContainerSpan = styled.span`
  width: auto;
  height: 23px;
  background-color: ${defaultTheme.spinner};
  visibility: ${({ show }) => (show ? "visible" : "hidden")};
  border-radius: 6px;
  padding: 8px 0px 8px 20px;
  position: absolute;
  display: flex;
  ::after {
    content: "";
    position: relative;
    top: ${({ position }) => (position === "top" ? "-141%" : "132%")};
    left: ${({ orientation, position }) =>
      orientation === "right" && position === "top"
        ? "-14%"
        : orientation === "right" && position === "bottom"
        ? "-14%"
        : "-72%"};
    transform: ${({ position }) =>
      position === "top" ? "rotate(0deg)" : "rotate(180deg)"};
    border-right: 12px solid transparent;
    border-top: 12px solid transparent;
    border-left: 12px solid transparent;
    border-bottom: 12px solid ${defaultTheme.spinner};
    display: ${({ position }) =>
      position === "top" ? "initial" : "-webkit-inline-box"};
  }
`;

const Texto = styled(Text)`
  ${({ mobile }) =>
    mobile
      ? defaultTheme.mobileFonts.body1Semibold
      : defaultTheme.desktopFonts.body1Semibold};
  color: #ffffff;
`;

const ContainerIcon = styled.div`
  float: left;
  padding-right: 10px;
`;
Tooltip.prototype = {
  /**
   *Arriba o abajo?
   */
  position: PropTypes.oneOf(["top", "bottom"]).isRequired,
  /**
   *Derecha o izquierda?
   */
  position: PropTypes.oneOf(["right", "left"]).isRequired,
  /**
   *Texto para mostrar
   */
  tooltipText: PropTypes.string.isRequired,
  /**
   *icon opcional
   */
  icon: PropTypes.object,
  /**
   * True para mostrar - False para ocultar
   */
   show: PropTypes.bool.isRequired,
};
