import { useState } from 'react';
import styled from 'styled-components';
import { Text } from '../Text/Text';
import PropTypes from "prop-types";

export const CardProduct = ({
    onPress,
    textLeft,
    textRight,
    font,
    fontPrice,
    IconLeft,
    IconRight,
    cursor,
    fontBody,
    titleBody,
    textBody,
    price,
    textBtnFooter
}) => {
    const [selected, setSelected] = useState(false);

    const onButtonPress = () => {
        onPress && onPress();

        setSelected(!selected);
    }

    return (
        <ContainerCard 
            onClick={() => onButtonPress()}
            selected={selected}
            cursor={cursor}
        >
            <ContainerCardHeader >
                <ContainerTextIcon  >
                    {IconLeft && <IconLeft selected={selected} />}
                    <TitleLabel
                        theme={font}
                        cursor={cursor}
                        selected={selected}
                    >{textLeft}</TitleLabel>
                </ContainerTextIcon>

                <ContainerTextIcon
                    cursor={cursor}
                    onClick={() => onButtonPress()}
                >
                    {textRight &&
                        <TitleBtn
                            theme={font}
                            cursor={cursor}
                            selected={selected}
                        >{textRight}</TitleBtn>
                    }
                    {IconRight &&
                        <IconRight   />
                    }
                </ContainerTextIcon>
            </ContainerCardHeader>

            <ContainerText>
                <TitleBody
                    theme={fontBody}
                    selected={selected}
                >{titleBody}</TitleBody>
                <TextBody
                    theme={fontBody}
                    selected={selected}
                ></TextBody>
            </ContainerText>

            <ContainerCardFooter>
                <ContainerPrice>
                    <Price
                        selected={selected}
                        theme={fontPrice}
                    >{price}</Price>
                </ContainerPrice>

                <BtnAdd
                    cursor={cursor}
                    onClick={() => onButtonPress()}
                    selected={selected}
                >{textBtnFooter}</BtnAdd>
            </ContainerCardFooter>
        </ContainerCard>
    )
}

const ContainerCard = styled.div`
    background-color: ${({ selected }) => (selected ? "#25807E" : "#FFF") };
    border: 1px solid rgba(0, 0, 0, 0.28);
    box-sizing: border-box;
    border-radius: 12px;
    padding: 16px;
    cursor: ${({ cursor }) => (cursor ? "pointer" : "default")};

    &:hover {
        background-color: ${({ selected }) => (selected ? "#1F5D57" : "#F5F5F5") };
    }

    &:active {
        background-color: #D6D6D6;
    }
`;

const ContainerCardHeader = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
`;

const ContainerTextIcon = styled.div`
    display: flex;
    align-items: center;
    gap: 17px;
    cursor: ${({ cursor }) => (cursor ? "pointer" : "default")};
`;

const ContainerText = styled.div`
    margin-top: 15px;
`;

const TitleLabel = styled(Text)`
    font-size: 12px;
    border: 1px solid ${({ selected }) => (selected ? "#fff" : "#000") };;
    box-sizing: border-box;
    border-radius: 6px;
    padding: 3px 6px;
    color: ${({ selected }) => (selected ? "#fff" : "#000") };
`;

const TitleBtn = styled(Text)`
    font-family: GilroyMedium;
    font-weight: 500;
    color: #000;
    font-size: 13px;
    cursor: ${({ cursor }) => (cursor ? "pointer" : "default")};
    color: ${({ selected }) => (selected ? "#fff" : "#000") };
`;

const TitleBody = styled(TitleBtn)`
    font-size: 18px;
    font-weight: 600;
`;

const TextBody = styled(TitleBody)`
    font-weight: 600;
    font-size: 13px;
`;

const ContainerCardFooter = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

const ContainerPrice = styled.div`

`;

const Price = styled(TextBody)`
    font-size: 18px;
    font-weight: 400;
`;

const BtnAdd = styled(Text)`
    font-size: 18px;
    color: ${({ selected }) => (selected ? "#fff" : "#5A9998") };
    cursor: ${({ cursor }) => (cursor ? "pointer" : "default")};
`;

CardProduct.propTypes = {
    /**
    * Evento al hacer click
    */
    onPress: PropTypes.func,
    /**
    * Texto de la derecha
    */
    textRight: PropTypes.string.isRequired,
    /**
    * Texto de la izquierda
    */
    textLeft: PropTypes.string.isRequired,
    /**
     * Color del texto
     */
    mobile: PropTypes.bool,
    /**
     * Fuente
     */
    font: PropTypes.string,
    /**
    * Texto del botón
    */
    IconLeft: PropTypes.func,
    /**
     * Icono del botón
     */
    IconRight: PropTypes.func,
    /**
    * Tipo de cursor
    */
    cursor: PropTypes.oneOf(['pointer', 'default']),
    /**
    * Fuente del body
    */
    fontBody: PropTypes.string,
};