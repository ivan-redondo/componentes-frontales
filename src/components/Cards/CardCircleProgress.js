import React from 'react';
import { COLORS } from '../../utils/Colors';
import styled from 'styled-components';

const STROKE_WIDTH = 5;
const CIRCLE_SIZE = 108;
const RADIUS = (CIRCLE_SIZE - STROKE_WIDTH) / 2;
const CIRCUMFERENCE = RADIUS * 2 * Math.PI;

export const CardCircleProgress = ({
    handleClick,
    dataProgress
}) => {
    const [offset, setOffset] = React.useState(0);

    React.useEffect(() => {
        
        setOffset(((100 - dataProgress) / 100) * CIRCUMFERENCE);
 
    }, [dataProgress]);

    return (
        <Container onClick={() => handleClick()}>
            <ProgressView>
                <Svg>
                    <circle x={0} y={0} stroke={COLORS.onSurfaceEnabled} fill={'none'} cy={CIRCLE_SIZE / 2} cx={CIRCLE_SIZE / 2} r={RADIUS} strokeWidth={STROKE_WIDTH / 2} />
                    <circle x={0} y={0} stroke={COLORS.primary} fill={'none'} cy={CIRCLE_SIZE / 2} cx={CIRCLE_SIZE / 2} r={RADIUS} strokeWidth={STROKE_WIDTH} strokeDasharray={CIRCUMFERENCE} strokeDashoffset={offset} strokeLinecap={'round'} transform={'rotate(-90 ' + CIRCLE_SIZE / 2 + ' ' + CIRCLE_SIZE / 2 + ')'} />
                </Svg>
            </ProgressView>
        </Container>
    );
};

const Container = styled.div`
  width: 150px;
  height: 198px;
  display: flex;
  flex-direction: column;
  align-items: center;
  cursor: pointer;
  justify-content: center;
`;
const ContainerNumber = styled.div`
  margin-top: 8px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: 22px;
`;
const NumberText = styled(Text)`
  font-style: normal;
  font-weight: bold;
  font-size: 15px;
  margin: 0;
  ${({ disabled }) => disabled && `color: ${COLORS.onSurfaceEnabled}`}
`;
const RemainDataText = styled(Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  text-align: center;
  margin: 0;
  color: ${COLORS.onSurfaceLow};
`;
const PendingText = styled(Text)`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  text-align: center;
  margin: 0;
  color: ${({ state }) => state ? getColorByState(state) : COLORS.secondaryYellow};
`;
const ViewPrincipal = styled.div`
  background-color: ${COLORS.primary};
  border-color: ${COLORS.primary};
  border-radius: 16px;
  border-width: 1px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 3px 8px;
  margin-top: 4px;
`;
const TextPrincipal = styled(Text)`
  font-size: 11px;
  font-weight: bold;
  text-align: center;
  margin: 0;
  color: ${COLORS.onSurface0};
`;
const ProgressView = styled.div`
  width: 108px;
  height: 108px;
  margin-top: 8px;
  position: relative;
`;
const CustomerView = styled.div`
  position: absolute;
  left: 14px;
  top: 14px;
  width: 80px;
  height: 80px;
  background-color: ${({ color }) => color ? color : COLORS.surface200};
  border-radius: 56px;
  align-items: center;
  justify-content: center;
  overflow: hidden;
`;
const Svg = styled.svg`
  width: 150px;
`;
