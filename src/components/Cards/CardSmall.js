import styled from 'styled-components';
import { Text } from '../Text/Text';
import PropTypes from "prop-types";

export const CardSmall = ({
    onPress,
    cursor,
    titleText,
    textBody,
    price,
    font,
}) => {
    const onButtonPress = () => {
        onPress && onPress();
    }

    return (
        <ContainerCard
            onClick={() => onButtonPress()}
            cursor={cursor}
        >
            <Title
                theme={font}
            >{titleText}</Title>
            <Price
            >{price}</Price>
            <TextBody
                theme={font}
            >{textBody}</TextBody>
        </ContainerCard>
    )
}

const ContainerCard = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    background: rgba(255, 255, 255, 0.24);
    border: 1px solid #D6D6D6;
    border-radius: 12px;
    padding: 16px 8px 16px 8px;
    width: 167px;
    cursor: ${({ cursor }) => (cursor ? "pointer" : "default")};
    text-align: center;

    &:hover {
        background-color: #F5F5F5;
    }

    &:active {
        background-color: #D6D6D6;
    }
`;

const Title = styled(Text)`
    font-size: 15px;
    font-weight: 400;
`;

const Price = styled(Text)`
    font-size: 24px;
    font-weight: 600;
`;

const TextBody = styled(Text)`
    font-size: 14px;
    font-weight: 400;
`;

CardSmall.propTypes = {
    /**
    * Evento al hacer click
    */
    onPress: PropTypes.func,
    /**
    * Tipo de cursor
    */
    cursor: PropTypes.oneOf(['pointer', 'default']),
    /**
     * Texto del titulo
     */
    titleText: PropTypes.string,
    /**
     * Texto del body
     */
    textBody: PropTypes.string,
    /**
     * Precio
     */
    price: PropTypes.string,
    /**
     * Fuente
     */
    font: PropTypes.string,
};