import { useState } from 'react';
import styled from 'styled-components';
import { Text } from '../Text/Text';
import PropTypes from "prop-types";

export const CardPrice = ({
    onPress,
    titleText,
    font,
    price,
    fontPrice,
    IconLeft,
    IconRight,
    cursor,
    iconBottomLeft
}) => {
    const [selected, setSelected] = useState(false);

    const onButtonPress = () => {
        onPress && onPress();

        setSelected(!selected);
    }

    return (
        <ContainerCard
            onClick={() => onButtonPress()}
            selected={selected}
        >
            <ContainerCardHeader>
                <ContainerTextIcon>
                    {IconLeft && <IconLeft selected={selected}/>}
                    <Title
                        theme={font}
                        cursor={cursor}
                        selected={selected}
                    >{titleText}</Title>
                </ContainerTextIcon>

                <ContainerTextIcon>
                    {IconRight &&
                        <IconRight />
                    }
                </ContainerTextIcon>
            </ContainerCardHeader>
            <ContainerBottom>
                <Price
                    theme={fontPrice}
                    selected={selected}
                >{price}</Price>
                <IconBottomLeft
                    selected={selected}
                >{iconBottomLeft}</IconBottomLeft>
            </ContainerBottom>
        </ContainerCard>
    )
}

const ContainerCard = styled.div`
    background-color: ${({ selected }) => (selected ? "#25807E" : "#FFF") };
    border: 1px solid rgba(0, 0, 0, 0.28);
    box-sizing: border-box;
    padding: 16px;
    border-radius: 16px;
    cursor: ${({ onClick }) => (onClick ? "pointer" : "default")};

    &:hover {
        background-color: ${({ selected }) => (selected ? "#1F5D57" : "#F5F5F5") };
    }

    &:active {
        background-color: #D6D6D6;
    }
`;

const ContainerCardHeader = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
`;

const ContainerTextIcon = styled.div`
    display: flex;
    align-items: center;
    gap: 17px;
`;

const ContainerBottom = styled.div`
    margin-top: 24px;
    display: flex;
    justify-content: space-between;
`;

const Title = styled(Text)`
    font-size: 15px;
    line-height: 22px;
    cursor: ${({ cursor }) => (cursor ? "pointer" : "default")};
    color: ${({ selected }) => (selected ? "#fff" : "#000") };
`;

const Price = styled(Text)`
    color: ${({ selected }) => (selected ? "#fff" : "#000") };
`;

const IconBottomLeft = styled(Text)`
    font-size: 12px;
    background: #FAB400;
    height: 22px;
    width: 52px;
    border-radius: 100px;
    padding: 3px 12px;
    display: flex;
    align-items: center;
    justify-content: center;
    color: ${({ selected }) => (selected ? "#fff" : "#000") };
`;

CardPrice.propTypes = {
    /**
    * Evento al hacer click
    */
    onPress: PropTypes.func,
    /**
    * Texto de la izquierda
    */
    titleText: PropTypes.string.isRequired,
    /**
     * Fuente
     */
    font: PropTypes.string,
    /**
     * Precio
     */
    price: PropTypes.string,
    /**
    * Fuente del precio
    */
    fontPrice: PropTypes.string,
    /**
     * Color del texto
     */
    IconLeft: PropTypes.func,
    /**
     * Icono del botón
     */
    IconRight: PropTypes.func,
    /**
    * Tipo de cursor
    */
    cursor: PropTypes.oneOf(['pointer', 'default']),
    /**
     * Icono del botón
     */
     iconBottomLeft: PropTypes.func,
};