import styled from 'styled-components';
import { Text } from '../Text/Text';
import PropTypes from "prop-types";

export const CardInfo = ({
    onPress,
    cursor,
    titleText,
    textBody,
    price,
    textBtnFooter,
    font,
    fontBody,
    previousPrice
}) => {
    const onButtonPress = () => {
        onPress && onPress();
    }

    return (
        <ContainerCard
            onClick={() => onButtonPress()}
            cursor={cursor}
        >
            <TitleBody
                theme={font}
                cursor={cursor}
            >{titleText}</TitleBody>

            <TextBody
                theme={fontBody}
            >{textBody}</TextBody>

            <ContainerCardFooter>

                <BtnAdd
                    cursor={cursor}
                    onClick={() => onButtonPress()}
                >{textBtnFooter}</BtnAdd>

                <ContainerPrice>
                    <PreviousPrice
                    >{previousPrice}</PreviousPrice>
                    <Price
                    >{price}</Price>
                </ContainerPrice>

            </ContainerCardFooter>
        </ContainerCard>
    )
}

const ContainerCard = styled.div`
    border: 1px solid rgba(0, 0, 0, 0.28);
    box-sizing: border-box;
    border-radius: 12px;
    padding: 16px;
    cursor: ${({ cursor }) => (cursor ? "pointer" : "default")};

    &:hover {
        background-color: #F5F5F5;
    }

    &:active {
        background-color: #D6D6D6;
    }
`;

const TitleBody = styled(Text)`
    font-size: 15px;
    font-weight: 600;
`;

const TextBody = styled(Text)`
    font-size: 15px;
`;

const ContainerCardFooter = styled.div`
    display: flex;
    flex-direction: row;
    align-items: flex-end;
    justify-content: space-between;
    margin-top: 40px;
`;

const ContainerPrice = styled.div`
    text-align: right;
`;

const Price = styled(Text)`
    font-size: 32px;
    font-weight: 600;
`;

const PreviousPrice = styled(Price)`
    text-decoration:line-through;
    font-size: 14px;
    font-weight: 400;
    color: rgba(0, 0, 0, 0.62);
`;

const BtnAdd = styled(Text)`
    font-size: 18px;
    color: #25807E;
    cursor: ${({ cursor }) => (cursor ? "pointer" : "default")};
`;

CardInfo.propTypes = {
    /**
    * Evento al hacer click
    */
    onPress: PropTypes.func,
    /**
    * Tipo de cursor
    */
    cursor: PropTypes.oneOf(['pointer', 'default']),
    /**
     * Texto del titulo
     */
    titleText: PropTypes.string,
    /**
     * Texto del body
     */
    textBody: PropTypes.string,
    /**
     * Precio
     */
    price: PropTypes.string,
     /**
     * Texto del botón
     */
    textBtnFooter: PropTypes.string,
    /**
     * FUente del texto
     */
    font: PropTypes.string,
    /**
    * Fuente del body
    */
    fontBody: PropTypes.string,
    /**
    * Precio anterior
    */
    previousPrice: PropTypes.string,
};