import styled from 'styled-components';
import { Text } from '../Text/Text';
import PropTypes from "prop-types";

export const CardDefault = ({ 
    onPress,
    textLeft,
    textRight,
    font,
    IconLeft,
    IconRight,
    cursor,
    fontBody,
    textBody
}) => {
    const onButtonPress = () => {
        onPress && onPress();
    }

    return (
        <ContainerCard
            onClick={() => onButtonPress()}
        >
            <ContainerCardHeader>
                <ContainerTextIcon>
                    {IconLeft && <IconLeft />}
                    <Title
                        theme={font}
                        cursor={cursor}
                    >{textLeft}</Title>
                </ContainerTextIcon>

                <ContainerTextIcon>
                    {textRight &&
                        <TitleBtn
                            theme={font}
                            cursor={cursor}
                        >{textRight}</TitleBtn>
                    }
                    {IconRight &&
                        <IconRight />
                    }
                </ContainerTextIcon>
            </ContainerCardHeader>
            <ContainerText>
                <TextBody
                    theme={fontBody}
                >{textBody}</TextBody>
            </ContainerText>
        </ContainerCard>
    )
}

const ContainerCard = styled.div`
    background-color: #fff;
    border: 1px solid #EBEBEB;
    box-shadow: 0px 2px 4px 1px rgba(32, 32, 32, 0.04);
    box-sizing: border-box;
    padding: 24px 16px;
    border-radius: 6px;
    cursor: ${({ onClick }) => (onClick ? "pointer" : "default")};
`;

const ContainerCardHeader = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
`;

const ContainerTextIcon = styled.div`
    display: flex;
    align-items: center;
    gap: 17px;
`;

const ContainerText = styled.div`
    padding-left: 40px;
    margin-top: 4px;
`;

const Title = styled(Text)`
    font-size: 15px;
    line-height: 22px;
    cursor: ${({ cursor }) => (cursor ? "pointer" : "default")};
`;

const TitleBtn = styled(Title)`
    color: #25807E;
`;

const TextBody = styled(Text)`
    font-size: 14px;
    font-weight: 400;
`;

CardDefault.propTypes = {
    /**
    * Evento al hacer click
    */
    onPress: PropTypes.func,
    /**
    * Texto de la derecha
    */
    textRight: PropTypes.string.isRequired,
    /**
    * Texto de la izquierda
    */
    textLeft: PropTypes.string.isRequired,
    /**
     * Color del texto
     */
    mobile: PropTypes.bool,
    /**
     * Fuente
     */
    font: PropTypes.string,
    /**
    * Texto del botón
    */
    IconLeft: PropTypes.func,
    /**
     * Icono del botón
     */
    IconRight: PropTypes.func,
    /**
    * Tipo de cursor
    */
    cursor: PropTypes.oneOf(['pointer', 'default']),
    /**
    * Fuente del body
    */
    fontBody: PropTypes.string,
};