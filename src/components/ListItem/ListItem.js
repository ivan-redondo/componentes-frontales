import React from "react";
import styled from "styled-components";
import { Text } from "../Text/Text";
import { SwitchItem } from "../Switch/SwitchItem";
import PropTypes from "prop-types";
import ChevronR from "../../assets/images/ChevronR";
import TickIcon from "../../assets/images/svg/TickIcon";

export const ListItem = ({
  type,
  Icon,
  leftLabel,
  leftBold,
  rightLabel,
  body,
  value,
  onClick,
  hideUnderline,
}) => {
  const [toggleValue, setToggleValue] = React.useState(false);

  React.useEffect(() => {
    setToggleValue(value);
  }, [value]);

  const changeValue = () => {
    setToggleValue(!toggleValue);
  };

  return (
    <Container
      hoverEnabled={
        onClick && type !== "checkbox" && type !== "radio" && type !== "tick"
          ? true
          : false
      }
    >
      {Icon && (
        <IconContainer>
          <Icon />
        </IconContainer>
      )}
      {type === "checkbox" && (
        <Checkbox
          value={toggleValue}
          onClick={() => {
            onClick && onClick();
            changeValue();
          }}
        >
          {toggleValue && <TickIcon color="#fff" width={16} height={14} />}
        </Checkbox>
      )}
      {type === "radio" && (
        <Radio
          value={toggleValue}
          onClick={() => {
            onClick && onClick();
            changeValue();
          }}
        >
          {toggleValue && <InnerRadio />}
        </Radio>
      )}
      <ListContainer type={type} hideUnderline={hideUnderline}>
        <LeftLabelContainer>
          <Text theme={leftBold ? "body1Semibold" : "body1Regular"}>
            {leftLabel}
          </Text>
          {body && <Text theme="body1Regular">{body}</Text>}
        </LeftLabelContainer>
        <RightLabelContainer>
          {type === "text" && <Text theme="body1Regular">{rightLabel}</Text>}
          {type === "textBtn" && (
            <Text
              theme="button2Semibold"
              cursor="pointer"
              onClick={() => {
                onClick && onClick();
              }}
            >
              {rightLabel}
            </Text>
          )}
        </RightLabelContainer>
        {type === "arrowTextBtn" && (
          <ArrowTextBtnContainer>
            <Text
              theme="button2Semibold"
              opacity={0.4}
              cursor="pointer"
              onClick={() => {
                onClick && onClick();
              }}
            >
              {rightLabel}
            </Text>
            <ArrowContainer>
              <ChevronR />
            </ArrowContainer>
          </ArrowTextBtnContainer>
        )}
        {type === "arrow" && (
          <ArrowTextBtnContainer
            onClick={() => {
              onClick && onClick();
            }}
          >
            <ChevronR />
          </ArrowTextBtnContainer>
        )}
        {type === "tick" && (
          <TickContainer>
            <TickIcon color="#fff" width={16} height={14} />
          </TickContainer>
        )}
        {type === "switch" && (
          <SwitchItem
            value={toggleValue}
            onClick={() => {
              onClick && onClick();
              changeValue();
            }}
          />
        )}
      </ListContainer>
    </Container>
  );
};

ListItem.propTypes = {
  /**
   * Tipo de elemento a mostrar: switch, textBtn, arrowTextBtn, text, arrow.
   */
  type: PropTypes.string,

  /**
   * Icono a la izquierda
   */
  Icon: PropTypes.func,
  /**
   * Texto a mostrar a la izquierda
   */
  leftLabel: PropTypes.string,
  /**
   * ¿Está en negrita el texto izquierdo?
   */
  leftBold: PropTypes.bool,
  /**
   * Texto a mostrar a la derecha
   */
  rightLabel: PropTypes.string,
  /**
   * Texto a mostrar en el cuerpo
   */
  body: PropTypes.string,
  /**
   * Si está activo o no el Switch
   */
  value: PropTypes.bool,
  /**
   * Función al clickar el switch / botón / flecha
   */
  onClick: PropTypes.func,

  /**
   * Oculta la barra inferior
   */
  hideUnderline: PropTypes.bool,
};

ListItem.defaultProps = {
  Icon: undefined,
  label: "",
  value: false,
  onClick: undefined,
};

const IconContainer = styled.div`
  padding-right: 16px;
  padding-bottom: 17px;
  height: 100%;
`;

const ArrowContainer = styled.div`
  padding-left: 14px;
`;

const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  align-items: flex-start;
  padding: 17px 16px 0px;
  border-radius: 6px;

  ${(props) =>
    props.hoverEnabled &&
    `
  &&:hover{
    background: #F5F5F5;
  }
  &&:active{
    background: #D6D6D6;
  }
  
  `}
`;

const ArrowTextBtnContainer = styled.div`
  display: flex;
  height: 100%;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  padding: 5px 0px 0px 24px;
`;

const TickContainer = styled.div`
  display: flex;
  width: 22px;
  height: 22px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background: #25807e;
  border-radius: 22px;
`;
const ListContainer = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  align-items: ${(props) => (props.type === "arrow" ? "center" : "flex-start")};
  justify-content: space-between;
  padding-bottom: 17px;
  ${(props) =>
    !props.hideUnderline && `border-bottom: 1px solid rgba(0, 0, 0, 0.09);`}
`;

const LeftLabelContainer = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const RightLabelContainer = styled.div`
  display: flex;
  flex: 1;
  align-items: flex-start;
  justify-content: flex-end;
`;

const Checkbox = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 15px;
  height: 15px;
  border-radius: 2px;
  margin-right: 16px;
  margin-bottom: 17px;
  cursor: pointer;
  ${(props) =>
    props.value
      ? `
        background: #1F5D57;
        border: 2px solid #1F5D57;

      `
      : `
        border: 2px solid #999898;
      `}
`;

const Radio = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 15px;
  height: 15px;
  border-radius: 15px;
  margin-right: 16px;
  margin-bottom: 17px;
  cursor: pointer;
  ${(props) =>
    props.value
      ? `
        border: 2px solid #25807E;

      `
      : `
        border: 2px solid #999898;
      `}
`;

const InnerRadio = styled.div`
  background: #25807e;
  width: 8px;
  height: 8px;
  border-radius: 8px;
`;
