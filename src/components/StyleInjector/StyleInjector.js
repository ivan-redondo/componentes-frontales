import React from 'react'
import styled from 'styled-components';
import globalStyles from '../../utils/globalStyles';
import PropTypes from 'prop-types';

export const StyleInjector = ({children}) => {


  return(
    <Container>
      {children}
    </Container>
  )

}

StyleInjector.propTypes = {

  /**
   * Este children abarcará el App de la aplicación objetivo, la cual contendrá los componentes del Storybook
   */
  children: PropTypes.object,

}

StyleInjector.defaultProps = {

  children: undefined,

}

const Container = styled.div`
  ${globalStyles}
`
