import CoreSansD25Light from'../assets/fonts/CoreSansD25Light.otf'
import CoreSansD35regular from'../assets/fonts/CoreSansD35Regular.otf'
import CoreSansD55Bold from'../assets/fonts/CoreSansD55Bold.otf'
import CoreSansD65heavy from'../assets/fonts/CoreSansD65Heavy.otf'
import RadomirLight from'../assets/fonts/Radomir-Tinkov-Gilroy-Light.otf'
import RadomirRegular from'../assets/fonts/Radomir-Tinkov-Gilroy-Regular.otf'
import RadomirBold from'../assets/fonts/Radomir-Tinkov-Gilroy-Bold.otf'
import RadomirHeavy from'../assets/fonts/Radomir-Tinkov-Gilroy-Heavy.otf'
import GilroyBlack from'../assets/fonts/Radomir-Tinkov-Gilroy-Black.otf'
import GilroyBlackItalic from'../assets/fonts/Radomir-Tinkov-Gilroy-BlackItalic.otf'
import GilroyBold from'../assets/fonts/Radomir-Tinkov-Gilroy-Bold.otf'
import GilroyBoldItalic from'../assets/fonts/Radomir-Tinkov-Gilroy-BoldItalic.otf'
import GilroyExtraBold from'../assets/fonts/Radomir-Tinkov-Gilroy-ExtraBold.otf'
import GilroyExtraBoldItalic from'../assets/fonts/Radomir-Tinkov-Gilroy-ExtraBoldItalic.otf'
import GilroyHeavy from'../assets/fonts/Radomir-Tinkov-Gilroy-Heavy.otf'
import GilroyHeavyItalic from'../assets/fonts/Radomir-Tinkov-Gilroy-HeavyItalic.otf'
import GilroyLight from'../assets/fonts/Radomir-Tinkov-Gilroy-Light.otf'
import GilroyLightItalic from'../assets/fonts/Radomir-Tinkov-Gilroy-LightItalic.otf'
import GilroyMedium from'../assets/fonts/Radomir-Tinkov-Gilroy-Medium.otf'
import GilroyMediumItalic from'../assets/fonts/Radomir-Tinkov-Gilroy-MediumItalic.otf'
import GilroyRegular from'../assets/fonts/Radomir-Tinkov-Gilroy-Regular.otf'
import GilroyRegularItalic from'../assets/fonts/Radomir-Tinkov-Gilroy-RegularItalic.otf'
import GilroySemiBold from'../assets/fonts/Radomir-Tinkov-Gilroy-SemiBold.otf'
import GilroySemiBoldItalic from'../assets/fonts/Radomir-Tinkov-Gilroy-SemiBoldItalic.otf'
import GilroyThin from'../assets/fonts/Radomir-Tinkov-Gilroy-Thin.otf'
import GilroyThinItalic from'../assets/fonts/Radomir-Tinkov-Gilroy-ThinItalic.otf'
import GilroyUltraLight from'../assets/fonts/Radomir-Tinkov-Gilroy-UltraLight.otf'
import GilroyUltraLightItalic from'../assets/fonts/Radomir-Tinkov-Gilroy-UltraLightItalic.otf'

const globalStyles = () => {

  return `
    body {
      padding: 0;
      margin: 0;
    }

    @font-face {
      font-family: CoreSansD25light;
      src: url(${CoreSansD25Light}) format("opentype");
      font-weight: lighter;
      font-style: normal;
    }
    
    @font-face {
      font-family: CoreSansD35regular;
      src: url(${CoreSansD35regular}) format("opentype");
    }
    
    @font-face {
      font-family: CoreSansD55Bold;
      src: url(${CoreSansD55Bold}) format("opentype");
      font-weight: normal;
      font-style: normal;
    }
    
    @font-face {
      font-family: CoreSansD65heavy;
      src: url(${CoreSansD65heavy}) format("opentype");
    }
    
    @font-face {
      font-family: RadomirLight;
      src: url(${RadomirLight}) format("opentype");
      font-weight: lighter;
      font-style: normal;
    }
    
    @font-face {
      font-family: RadomirRegular;
      src: url(${RadomirRegular}) format("opentype");
    }
    
    @font-face {
      font-family: RadomirBold;
      src: url(${RadomirBold}) format("opentype");
      font-weight: normal;
      font-style: normal;
    }
    
    @font-face {
      font-family: RadomirHeavy;
      src: url(${RadomirHeavy}) format("opentype");
    }
    
    @font-face {
      font-family: GilroyBlack;
      src: url(${GilroyBlack}) format("opentype");
    }
    @font-face {
      font-family: GilroyBlackItalic;
      src: url(${GilroyBlackItalic}) format("opentype");
    }
    
    @font-face {
      font-family: GilroyBold;
      src: url(${GilroyBold}) format("opentype");
    }
    
    @font-face {
      font-family: GilroyBoldItalic;
      src: url(${GilroyBoldItalic}) format("opentype");
    }
    
    @font-face {
      font-family: GilroyExtraBold;
      src: url(${GilroyExtraBold}) format("opentype");
    }
    
    @font-face {
      font-family: GilroyExtraBoldItalic;
      src: url(${GilroyExtraBoldItalic}) format("opentype");
    }
    
    @font-face {
      font-family: GilroyHeavy;
      src: url(${GilroyHeavy}) format("opentype");
    }
    
    @font-face {
      font-family: GilroyHeavyItalic;
      src: url(${GilroyHeavyItalic}) format("opentype");
    }
    
    @font-face {
      font-family: GilroyLight;
      src: url(${GilroyLight}) format("opentype");
    }
    
    @font-face {
      font-family: GilroyLightItalic;
      src: url(${GilroyLightItalic}) format("opentype");
    }
    
    @font-face {
      font-family: GilroyMedium;
      src: url(${GilroyMedium}) format("opentype");
    }
    
    
    @font-face {
      font-family: GilroyMediumItalic;
      src: url(${GilroyMediumItalic}) format("opentype");
    }
    
    
    @font-face {
      font-family: GilroyRegular;
      src: url(${GilroyRegular}) format("opentype");
    }
    
    
    @font-face {
      font-family: GilroyRegularItalic;
      src: url(${GilroyRegularItalic}) format("opentype");
    }
    
    
    @font-face {
      font-family: GilroySemiBold;
      src: url(${GilroySemiBold}) format("opentype");
    }
    
    
    @font-face {
      font-family: GilroySemiBoldItalic;
      src: url(${GilroySemiBoldItalic}) format("opentype");
    }
    
    
    @font-face {
      font-family: GilroyThin;
      src: url(${GilroyThin}) format("opentype");
    }
    
    
    @font-face {
      font-family: GilroyThinItalic;
      src: url(${GilroyThinItalic}) format("opentype");
    }
    
    
    @font-face {
      font-family: GilroyUltraLight;
      src: url(${GilroyUltraLight}) format("opentype");
    }
    
    @font-face {
      font-family: GilroyUltraLightItalic;
      src: url(${GilroyUltraLightItalic}) format("opentype");
    }
  `;

}

export default globalStyles;