export const defaultTheme = {
    name: "defaultTheme",
    primary: "#000000",
    pale: "#FFFFFF",
    gridBackground: "#FFFFFF",
    menuBackground: "#FFFFFF",
    white: "#FFFFFF",
    grey: "#221f2c", //Alt TV Shade
    border: "#221f2c", //Alt TV Shade
    hoverbutton: "",
    light: "#d5fbf2", //Alt Gas Shade
    secondLight: "#97f5e0", //Alt Gas
    alt_luz: "#e3d600", //amarillo
    alt_fibra: "#e47373", //rojo
    alt_tv: "#aa9ddf", //lila
    alt_movil: "#33B2B0", //turquesa oscuro
    alt_gas: "#97f5e0", //verde agua
    regularText: "#051111", //ALT Movil Shade2
    primaryChart: "#97f5e0", //Alt Gas
    secondaryChart: "#d5fbf2", //Alt Gas Shade
    chartDark: "#E3D600",
    chartMedium: "rgba(227, 214, 0, 0.5)",
    chartLight: "rgba(227, 214, 0, 0.25)",
    gas: "#c4786d",
    gasPale: "#efa59b",
    red: "#ff416d",
    sideMenuButtonEnabled: "#292929",
    disabledButtonBackColor: "#9ccdd5",
    updateButtonBackColor: "rgba(61,139,157,1)",
    buttonColor: "#051111",
    spinner: "#25807E",
    graphBackGroundDark: "#1DC9A2",
    graphBackGroundMedium: "rgba(29, 201, 162, 0.5)",
    graphBackGroundLight: "rgba(29, 201, 162, 0.25)",
    lightFont: "RadomirLight",
    regularFont: "RadomirRegular",
    boldFont: "RadomirBold;",
    heavyFont: "RadomirHeavy",
    baseFontSize: "1.5em",
    tableShortHeight: "30vh",
    tableMediumHeight: "40vh",
    tableFullHeight: "70vh",
    whiteLogo: "Logo_A_Black.png",
    subLogo: "Logo_alterna_Black.png",
    contractLogo: "download_contract_icon_integral.png",
    mobile: "767px",
    mobileFonts: {
      h1Semibold: `
        font-family: GilroySemiBold;
        font-size: 32px;
        font-style: normal;
        font-weight: 700;
        line-height: 38px;
        letter-spacing: -0.01em;
        text-align: left;
      
      `, 
      h1Medium: `
        font-family: GilroyMedium;
        font-size: 32px;
        font-style: normal;
        font-weight: 400;
        line-height: 38px;
        letter-spacing: -0.02em;
        text-align: left;
      
      `,
      h1Regular: `
        font-family: GilroyRegular;
        font-size: 32px;
        font-style: normal;
        font-weight: 400;
        line-height: 38px;
        letter-spacing: -0.02em;
        text-align: left;
      `,
      h2Semibold: `
      font-family: GilroySemiBold;
      font-size: 24px;
      font-style: normal;
      font-weight: 600;
      line-height: 30px;
      letter-spacing: -0.01em;
      text-align: left;
      `,
      h2Medium: `
        font-family: GilroyMedium;
        font-size: 24px;
        font-style: normal;
        font-weight: 400;
        line-height: 30px;
        letter-spacing: -0.01em;
        text-align: left;
      `,
      h2Regular: `
      font-family: GilroyRegular;
      font-size: 24px;
      font-style: normal;
      font-weight: 400;
      line-height: 30px;
      letter-spacing: -0.01em;
      text-align: left;
      `,
      h3Semibold:`
      font-family: GilroySemiBold;
      font-size: 20px;
      font-style: normal;
      font-weight: 600;
      line-height: 26px;
      letter-spacing: 0em;
      text-align: left;
      `,
      h3Medium:`
      font-family: GilroyMedium;
      font-size: 20px;
      font-style: normal;
      font-weight: 400;
      line-height: 26px;
      letter-spacing: 0em;
      text-align: left;
      `,
      h3Regular: `
      font-family: GilroyRegular;
      font-size: 20px;
      font-style: normal;
      font-weight: 400;
      line-height: 26px;
      letter-spacing: 0em;
      text-align: left;
      `,
      h4Semibold: `
      font-family: GilroySemiBold;
      font-size: 18px;
      font-style: normal;
      font-weight: 600;
      line-height: 24px;
      letter-spacing: -0.02em;
      text-align: left;
      `,
      h4Medium: `
      font-family: GilroyMedium;
      font-size: 18px;
      font-style: normal;
      font-weight: 500;
      line-height: 24px;
      letter-spacing: -0.02em;
      text-align: left;
      `,
      h4Regular: `
      font-family: GilroyRegular;
      font-size: 18px;
      font-style: normal;
      font-weight: 400;
      line-height: 24px;
      letter-spacing: -0.01em;
      text-align: left;
      `,
      hButtonSemibold: `
        font-family: GilroySemiBold;
        font-size: 14px;
        font-style: normal;
        font-weight: 600;
        line-height: 22px;
        letter-spacing: 0em;
        text-align: left;
        color: #25807E;
      `,
      hButtonMedium: `
        font-family: GilroyMedium;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 22px;
        letter-spacing: 0em;
        text-align: left;
        color: #25807E;
      `,
      hButtonRegular: `
        font-family: GilroyRegular;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 22px;
        letter-spacing: 0em;
        text-align: left;
        color: #25807E;
      `,
      button1Semibold: `
        font-family: GilroySemiBold;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
        line-height: 18px;
        letter-spacing: 0em;
        text-align: left;
        color: #25807E;
      `,
      button1Medium: `
        font-family: GilroyMedium;
        font-size: 16px;
        font-style: normal;
        font-weight: 400;
        line-height: 18px;
        letter-spacing: 0em;
        text-align: left;
        color: #25807E;
      `, 
      button1Regular: `
        font-family: GilroyRegular;
        font-size: 16px;
        font-style: normal;
        font-weight: 400;
        line-height: 18px;
        letter-spacing: 0em;
        text-align: left;
        color: #25807E;
      `,
      button2Semibold: `
        font-family: GilroySemiBold;
        font-size: 14px;
        font-style: normal;
        font-weight: 600;
        line-height: 16px;
        letter-spacing: 0em;
        text-align: left;
        color: #25807E;
      `,
      button2Medium: `
        font-family: GilroyMedium;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 16px;
        letter-spacing: 0em;
        text-align: left;
        color: #25807E;
      `, 
      button2Regular: `
        font-family: GilroyRegular;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 16px;
        letter-spacing: 0em;
        text-align: left;
        color: #25807E;
      `,
      body1Semibold: `
        font-family: GilroySemiBold;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
        line-height: 23px;
        letter-spacing: 0em;
        text-align: left;
      `,
      body1Medium: `
        font-family: GilroyMedium;
        font-size: 16px;
        font-style: normal;
        font-weight: 400;
        line-height: 23px;
        letter-spacing: 0em;
        text-align: left;
      `,
      body1Regular: `
        font-family: GilroyRegular;
        font-size: 16px;
        font-style: normal;
        font-weight: 400;
        line-height: 23px;
        letter-spacing: 0em;
        text-align: left;
  
      `,
      body1RegularSlashed: `
        font-family: GilroyRegular;
        font-size: 16px;
        font-style: normal;
        font-weight: 400;
        line-height: 23px;
        letter-spacing: 0em;
        text-align: left;
        text-decoration-line: line-through;
      `,
      body2Semibold: `
        font-family: GilroySemiBold;
        font-size: 14px;
        font-style: normal;
        font-weight: 600;
        line-height: 20px;
        letter-spacing: 0em;
        text-align: left;
      `,
      body2Medium: `
      font-family: GilroyMedium;
      font-size: 14px;
      font-style: normal;
      font-weight: 500;
      line-height: 20px;
      letter-spacing: 0em;
      text-align: left;
      `,
      body2Regular: `
      font-family: GilroyRegular;
      font-size: 14px;
      font-style: normal;
      font-weight: 400;
      line-height: 20px;
      letter-spacing: 0em;
      text-align: left;
      `,
      body2RegularSlashed: `
      font-family: GilroyRegular;
      font-size: 14px;
      font-style: normal;
      font-weight: 400;
      line-height: 20px;
      letter-spacing: 0em;
      text-align: left;
      text-decoration-line: line-through;
      `,
      overline1Semibold:`
        font-family: GilroySemiBold;
        font-size: 12px;
        font-style: normal;
        font-weight: 600;
        line-height: 16px;
        letter-spacing: 0.02em;
        text-align: left;
        text-transform: uppercase;
      `,
      overline1Medium:`
        font-family: GilroyMedium;
        font-size: 12px;
        font-style: normal;
        font-weight: 500;
        line-height: 16px;
        letter-spacing: 0.02em;
        text-align: left;
        text-transform: uppercase;
      `,
      overline1Regular:`
        font-family: GilroyRegular;
        font-size: 12px;
        font-style: normal;
        font-weight: 400;
        line-height: 16px;
        letter-spacing: 0.02em;
        text-align: left;
        text-transform: uppercase;
      `,
      overline2Semibold: `
        font-family: GilroySemiBold;
        font-size: 11px;
        font-style: normal;
        font-weight: 600;
        line-height: 14px;
        letter-spacing: 0.02em;
        text-align: left;
        text-transform: uppercase;
  
      `,
      overline2Medium: `
      font-family: GilroyMedium;
      font-size: 11px;
      font-style: normal;
      font-weight: 500;
      line-height: 14px;
      letter-spacing: 0.02em;
      text-align: left;
      text-transform: uppercase;
  
      `,
      overline2Regular: `
        font-family: GilroyRegular;
        font-size: 11px;
        font-style: normal;
        font-weight: 400;
        line-height: 14px;
        letter-spacing: 0.02em;
        text-align: left;
        text-transform: uppercase;
  
      `,
  
      overline3Semibold: `
        font-family: GilroySemiBold;
        font-size: 10px;
        font-style: normal;
        font-weight: 600;
        line-height: 14px;
        letter-spacing: 0.02em;
        text-align: left;
        text-transform: uppercase;
  
      `,
      overline3Medium: `
        font-family: GilroyMedium;
        font-size: 10px;
        font-style: normal;
        font-weight: 500;
        line-height: 14px;
        letter-spacing: 0.02em;
        text-align: left;
        text-transform: uppercase;
      
      `,
      overline3Regular: `
        font-family: GilroyRegular;
        font-size: 10px;
        font-style: normal;
        font-weight: 400;
        line-height: 14px;
        letter-spacing: 0.02em;
        text-align: left;
        text-transform: uppercase;
      `,
  
      caption1Semibold:`
      font-family: GilroySemiBold;
      font-size: 12px;
      font-style: normal;
      font-weight: 600;
      line-height: 16px;
      letter-spacing: 0em;
      text-align: left;
  
      `,
      caption1Medium:`
      font-family: GilroyMedium;
      font-size: 12px;
      font-style: normal;
      font-weight: 400;
      line-height: 16px;
      letter-spacing: 0em;
      text-align: left;
  
      `,
      caption1Regular:`
      font-family: GilroyRegular;
      font-size: 12px;
      font-style: normal;
      font-weight: 400;
      line-height: 16px;
      letter-spacing: 0em;
      text-align: left;
  
      `,
  
      caption2Semibold:`
      font-family: GilroySemiBold;
      font-size: 11px;
      font-style: normal;
      font-weight: 600;
      line-height: 16px;
      letter-spacing: 0em;
      text-align: left;
  
      `,
      caption2Medium:`
      font-family: GilroyMedium;
      font-size: 11px;
      font-style: normal;
      font-weight: 500;
      line-height: 16px;
      letter-spacing: 0em;
      text-align: left;
  
      `,
      caption2Regular:`
      font-family: GilroyRegular;
      font-size: 11px;
      font-style: normal;
      font-weight: 400;
      line-height: 16px;
      letter-spacing: 0em;
      text-align: left;
  
      `,
      caption3Semibold:`
      font-family: GilroySemiBold;
      font-size: 8px;
      font-style: normal;
      font-weight: 600;
      line-height: 11px;
      letter-spacing: 0em;
      text-align: left;
  
      `,
      caption3Medium:`
      font-family: GilroyMedium;
      font-size: 8px;
      font-style: normal;
      font-weight: 500;
      line-height: 11px;
      letter-spacing: 0em;
      text-align: left;
      `,
      caption3Regular:`
      font-family: GilroyRegular;
      font-size: 8px;
      font-style: normal;
      font-weight: 400;
      line-height: 11px;
      letter-spacing: 0em;
      text-align: left;
      `,
    },
  
    desktopFonts: {
      h1Semibold: `
        font-family: GilroySemiBold;
        font-size: 32px;
        font-style: normal;
        font-weight: 600;
        line-height: 38px;
        letter-spacing: -0.02em;
        text-align: left;
      `, 
      h1Medium: `
        font-family: GilroyMedium;
        font-size: 32px;
        font-style: normal;
        font-weight: 500;
        line-height: 38px;
        letter-spacing: -0.02em;
        text-align: left;
      `,
      h1Regular: `
        font-family: GilroyRegular;
        font-size: 32px;
        font-style: normal;
        font-weight: 400;
        line-height: 38px;
        letter-spacing: -0.02em;
        text-align: left;
      `,
      h2Semibold: `
      font-family: GilroySemiBold;
      font-size: 24px;
      font-style: normal;
      font-weight: 600;
      line-height: 30px;
      letter-spacing: -0.01em;
      text-align: left;
      `,
      h2Medium: `
        font-family: GilroyMedium;
        font-size: 24px;
        font-style: normal;
        font-weight: 500;
        line-height: 30px;
        letter-spacing: -0.01em;
        text-align: left;
      `,
      h2Regular: `
      font-family: GilroyRegular;
      font-size: 24px;
      font-style: normal;
      font-weight: 400;
      line-height: 30px;
      letter-spacing: -0.01em;
      text-align: left;
      `,
      h3Semibold:`
      font-family: GilroySemiBold;
      font-size: 20px;
      font-style: normal;
      font-weight: 600;
      line-height: 26px;
      letter-spacing: 0em;
      text-align: left;
      `,
      h3Medium:`
      font-family: GilroyMedium;
      font-size: 20px;
      font-style: normal;
      font-weight: 500;
      line-height: 26px;
      letter-spacing: 0em;
      text-align: left;
      `,
      h3Regular: `
      font-family: GilroyRegular;
      font-size: 20px;
      font-style: normal;
      font-weight: 400;
      line-height: 26px;
      letter-spacing: 0em;
      text-align: left;
      `,
      h4Semibold: `
      font-family: GilroySemiBold;
      font-size: 18px;
      font-style: normal;
      font-weight: 600;
      line-height: 24px;
      letter-spacing: -0.02em;
      text-align: left;
      `,
      h4Medium: `
      font-family: GilroyMedium;
      font-size: 18px;
      font-style: normal;
      font-weight: 500;
      line-height: 24px;
      letter-spacing: -0.02em;
      text-align: left;
      `,
      h4Regular: `
      font-family: GilroyRegular;
      font-size: 18px;
      font-style: normal;
      font-weight: 400;
      line-height: 24px;
      letter-spacing: -0.01em;
      text-align: left;
      `,
      hButtonSemibold: `
        font-family: GilroySemiBold;
        color: #25807E;
        font-size: 18px;
        font-style: normal;
        font-weight: 600;
        line-height: 22px;
        letter-spacing: 0em;
        text-align: left;
      `,
      hButtonMedium: `
        font-family: GilroyMedium;
        color: #25807E;
        font-size: 18px;
        font-style: normal;
        font-weight: 400;
        line-height: 22px;
        letter-spacing: 0em;
        text-align: left;
  
      `,
      hButtonRegular: `
        font-family: GilroyRegular;
        color: #25807E;
        font-size: 18px;
        font-style: normal;
        font-weight: 400;
        line-height: 22px;
        letter-spacing: 0em;
        text-align: left;
      `,
      button1Semibold: `
        font-family: GilroySemiBold;
        color: #25807E;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
        line-height: 18px;
        letter-spacing: 0em;
        text-align: left;
  
      `,
      button1Medium: `
        font-family: GilroyMedium;
        color: #25807E;
        font-size: 16px;
        font-style: normal;
        font-weight: 400;
        line-height: 18px;
        letter-spacing: 0em;
        text-align: left;
      `, 
      button1Regular: `
        font-family: GilroyRegular;
        color: #25807E;
        font-size: 16px;
        font-style: normal;
        font-weight: 400;
        line-height: 18px;
        letter-spacing: 0em;
        text-align: left;
      `,
      button2Semibold: `
        font-family: GilroySemiBold;
        color: #25807E;
        font-size: 14px;
        font-style: normal;
        font-weight: 600;
        line-height: 16px;
        letter-spacing: 0em;
        text-align: left;
  
      `,
      button2Medium: `
        font-family: GilroyMedium;
        color: #25807E;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 16px;
        letter-spacing: 0em;
        text-align: left;
      `, 
      button2Regular: `
        font-family: GilroyRegular;
        color: #25807E;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 16px;
        letter-spacing: 0em;
        text-align: left;
      `,
      body1Semibold: `
        font-family: GilroySemiBold;
        font-size: 15px;
        font-style: normal;
        font-weight: 600;
        line-height: 22px;
        letter-spacing: 0em;
        text-align: left;
      `,
      body1Medium: `
        font-family: GilroyMedium;
        font-size: 15px;
        font-style: normal;
        font-weight: 400;
        line-height: 22px;
        letter-spacing: 0em;
        text-align: left;
      `,
      body1Regular: `
        font-family: GilroyRegular;
        font-size: 15px;
        font-style: normal;
        font-weight: 400;
        line-height: 22px;
        letter-spacing: 0em;
        text-align: left;
      `,
      body1RegularSlashed: `
        font-family: GilroyRegular;
        text-decoration-line: line-through;
        font-size: 15px;
        font-style: normal;
        font-weight: 400;
        line-height: 22px;
        letter-spacing: 0em;
        text-align: left;
      `,
      body2Semibold: `
        font-family: GilroySemiBold;
        font-size: 14px;
        font-style: normal;
        font-weight: 600;
        line-height: 20px;
        letter-spacing: 0em;
        text-align: left; 
      `,
      body2Medium: `
      font-family: GilroyMedium;
      font-size: 14px;
      font-style: normal;
      font-weight: 400;
      line-height: 20px;
      letter-spacing: 0em;
      text-align: left;
      `,
      body2Regular: `
      font-family: GilroyRegular;
      font-size: 14px;
      font-style: normal;
      font-weight: 400;
      line-height: 20px;
      letter-spacing: 0em;
      text-align: left;
  
      `,
      body2RegularSlashed: `
      font-family: GilroyRegular;
      text-decoration-line: line-through;
      font-size: 14px;
      font-style: normal;
      font-weight: 400;
      line-height: 20px;
      letter-spacing: 0em;
      text-align: left;
      `,
      overline1Semibold:`
        font-family: GilroySemiBold;
        text-transform: uppercase;
        font-size: 12px;
        font-style: normal;
        font-weight: 600;
        line-height: 16px;
        letter-spacing: 0.02em;
        text-align: left;
      `,
      overline1Medium:`
        font-family: GilroyMedium;
        text-transform: uppercase;
        font-size: 12px;
        font-style: normal;
        font-weight: 400;
        line-height: 16px;
        letter-spacing: 0.02em;
        text-align: left;
      `,
      overline1Regular:`
        font-family: GilroyRegular;
        text-transform: uppercase;
        font-size: 12px;
        font-style: normal;
        font-weight: 400;
        line-height: 16px;
        letter-spacing: 0.02em;
        text-align: left;
      `,
      overline2Semibold: `
        font-family: GilroySemiBold;
        text-transform: uppercase;
        font-size: 11px;
        font-style: normal;
        font-weight: 600;
        line-height: 14px;
        letter-spacing: 0.02em;
        text-align: left;
      `,
      overline2Medium: `
      font-family: GilroyMedium;
      text-transform: uppercase;
      font-size: 11px;
      font-style: normal;
      font-weight: 400;
      line-height: 14px;
      letter-spacing: 0.02em;
      text-align: left;
      `,
      overline2Regular: `
        font-family: GilroyRegular;
        text-transform: uppercase;
        font-size: 11px;
        font-style: normal;
        font-weight: 400;
        line-height: 14px;
        letter-spacing: 0.02em;
        text-align: left;  
      `,
  
      overline3Semibold: `
        font-family: GilroySemiBold;
        text-transform: uppercase;
        font-size: 10px;
        font-style: normal;
        font-weight: 600;
        line-height: 14px;
        letter-spacing: 0.02em;
        text-align: left;
      `,
      overline3Medium: `
        font-family: GilroyMedium;
        text-transform: uppercase;
        font-size: 10px;
        font-style: normal;
        font-weight: 500;
        line-height: 14px;
        letter-spacing: 0.02em;
        text-align: left;
      `,
      overline3Regular: `
        font-family: GilroyRegular;
        text-transform: uppercase;
        font-size: 10px;
        font-style: normal;
        font-weight: 400;
        line-height: 14px;
        letter-spacing: 0.02em;
        text-align: left;
      `,
  
      caption1Semibold:`
      font-family: GilroySemiBold;
      font-size: 12px;
      font-style: normal;
      font-weight: 600;
      line-height: 16px;
      letter-spacing: 0em;
      text-align: left;
      `,
      caption1Medium:`
      font-family: GilroyMedium;
      font-size: 12px;
      font-style: normal;
      font-weight: 500;
      line-height: 16px;
      letter-spacing: 0em;
      text-align: left;
      `,
      caption1Regular:`
      font-family: GilroyRegular;
      font-size: 12px;
      font-style: normal;
      font-weight: 400;
      line-height: 16px;
      letter-spacing: 0em;
      text-align: left;
      `,
  
      caption2Semibold:`
      font-family: GilroySemiBold;
      font-size: 11px;
      font-style: normal;
      font-weight: 600;
      line-height: 16px;
      letter-spacing: 0em;
      text-align: left;
      `,
      caption2Medium:`
      font-family: GilroyMedium;
      font-size: 11px;
      font-style: normal;
      font-weight: 500;
      line-height: 16px;
      letter-spacing: 0em;
      text-align: left;
      `,
      caption2Regular:`
      font-family: GilroyRegular;
      font-size: 11px;
      font-style: normal;
      font-weight: 400;
      line-height: 16px;
      letter-spacing: 0em;
      text-align: left;
      `,
      caption3Semibold:`
      font-family: GilroySemiBold;
      font-size: 8px;
      font-style: normal;
      font-weight: 600;
      line-height: 11px;
      letter-spacing: 0em;
      text-align: left;
      `,
      caption3Medium:`
      font-family: GilroyMedium;
      font-size: 8px;
      font-style: normal;
      font-weight: 500;
      line-height: 11px;
      letter-spacing: 0em;
      text-align: left;
      `,
      caption3Regular:`
      font-family: GilroyRegular;
      font-size: 8px;
      font-style: normal;
      font-weight: 400;
      line-height: 11px;
      letter-spacing: 0em;
      text-align: left;
      `,
    }
  }