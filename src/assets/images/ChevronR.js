import React from "react";

const ChevronR = () => {

  return(
    <svg width="6" height="10" viewBox="0 0 6 10" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd" clipRule="evenodd" d="M0.195262 0.528575C0.455612 0.268226 0.877722 0.268226 1.13807 0.528575L5.13807 4.52858C5.39842 4.78892 5.39842 5.21103 5.13807 5.47138L1.13807 9.47138C0.877722 9.73173 0.455612 9.73173 0.195262 9.47138C-0.0650874 9.21103 -0.0650874 8.78893 0.195262 8.52858L3.72386 4.99998L0.195262 1.47138C-0.0650874 1.21103 -0.0650874 0.788925 0.195262 0.528575Z" fill="black"/>
    </svg>
  )

}

export default ChevronR;