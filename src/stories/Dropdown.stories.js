import {Dropdown} from '../components/Dropdown/Dropdown';
import {Text}  from '../components/Text/Text';

export default {
  title: 'Alterna/Dropdown',
  component: Dropdown,
};

const Template = (args) => <Dropdown {...args}><Text theme="h1Semibold" label="prueba"/></Dropdown>;

export const Base = Template.bind({});
Base.args = {
  onDropdownClick: null,
  label1: "Texto",
  label2: "",
  opened: false,
  AuxiliarIcon: undefined,
  onAuxiliarIconClick: null,
  mobile: false,
};
