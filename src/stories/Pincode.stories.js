
import { action } from '@storybook/addon-actions';
import { PincodeInput } from '../components/Pincode';
import { PincodeGroup } from '../components/Pincode/PincodeGroup';

export default {
  title: "Alterna/PinCode",
  component: PincodeInput,
  subcomponents: {PincodeGroup},
  decorators: [(Story) => (
    <div style={{width: "300px"}}>
      <Story />
    </div>
  )]
};

const PincodeInpuTemplate = (args) => <PincodeInput {...args} />;
const PincodeGroupTemplate = (args) => <PincodeGroup {...args} />;

export const PinInput = PincodeInpuTemplate.bind({});
PinInput.args = {
  defaultVal: 0,
  onChange: action('onChange')
};

export const PinGroup = PincodeGroupTemplate.bind({});
PinGroup.args = {
  quantity: 4,
};
