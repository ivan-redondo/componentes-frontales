import { CardDefault } from "../components/Cards/CardDefault";
import { CardProduct } from "../components/Cards/CardProduct";
import { CardInfo } from "../components/Cards/CardInfo";
import { CardSmall } from "../components/Cards/CardSmall";
import { CardPrice } from "../components/Cards/CardPrice";
import { CardCircleProgress } from '../components/Cards/CardCircleProgress'
import { action } from '@storybook/addon-actions';
import Interrogation from '../assets/images/svg/Interrogation';
import Info from '../assets/images/svg/Info';
import Package from '../assets/images/svg/Package';

export default {
  title: 'Alterna/Cards',
  component: CardDefault,
  subcomponents: { CardProduct },
};

const Template = (args) => <CardDefault {...args} />;
const CardProductTemplate = (args) => <CardProduct {...args} />;
const CardInfoTemplate = (args) => <CardInfo {...args} />;
const CardSmallTemplate = (args) => <CardSmall {...args} />;
const CardPriceTemplate = (args) => <CardPrice {...args} />;
const CardsCircleTemplate = (args) => <CardCircleProgress {...args} />;

export const Default = Template.bind({});
Default.args = {
  onPress: action('onClick'),
  textLeft: "Title",
  textRight: "Editar",
  font: "h1Semibold",
  IconLeft: Interrogation,
  IconRight: Info,
  cursor: "default",
  fontBody: "body1Regular",
  textBody: "body"
};

export const CardProducts = CardProductTemplate.bind({});
CardProducts.args = {
  onPress: action('onClick'),
  textLeft: "NUEVO",
  textRight: "Más info",
  font: "h4Semibold",
  fontPrice: "h4Regular",
  IconLeft: Package,
  IconRight: Info,
  cursor: "default",
  fontBody: "h3Semibold",
  titleBody: "Name product",
  textBody: "Promo 1",
  price: "00,00 €",
  textBtnFooter: "Añadir"
};

export const CardInformation = CardInfoTemplate.bind({});
CardInformation.args = {
  onPress: action('onClick'),
  cursor: "default",
  titleText: "Fibra 600Mbps | Móvil 30GB",
  textBody: "+ Fijo con llamadas ilimitadas",
  price: "00,00 €",
  textBtnFooter: "Más información",
  font: 'body1Semibold',
  fontBody: "body1Regular",
  previousPrice: "00,00 €"
};

export const CardsSmall = CardSmallTemplate.bind({});
CardsSmall.args = {
  onPress: action('onClick'),
  cursor: "default",
  titleText: "Title",
  textBody: "Text",
  price: "0,00X",
  font: "body1Regular",
};

export const CardsPrice = CardPriceTemplate.bind({});
CardsPrice.args = {
  onPress: action('onClick'),
  titleText: "Title",
  font: "h4Semibold",
  price: "0,00€/mes",
  fontPrice: "h1Semibold",
  IconLeft: Interrogation,
  IconRight: Info,
  cursor: "default",
  iconBottomLeft: "CHIP"
};

export const CardsCircle = CardsCircleTemplate.bind({});
CardsCircle.args = {
  handleClick: action('onClick'),
  subscription: {consumption: 1},
  dataProgress: 325,
};