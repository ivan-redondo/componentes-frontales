import { Tooltip }  from '../components/Tooltip/Tooltip';
import IconTooltip from '../assets/images/svg/IconTooltip';

 export default {
  component: Tooltip,
  title: 'Alterna/Tooltip',
};

const Template = (args) => <Tooltip {...args} />;

export const Default = Template.bind({});
Default.args = {
    tooltipText: 'Text',
    position: 'top',
    orientation: 'right',
    icon: <IconTooltip/>,
    show: true
};