
import { action } from '@storybook/addon-actions';
import Phone from "../assets/images/svg/Phone";
import { ListItem } from '../components/ListItem/ListItem';

export default {
  title: "Alterna/ListItem",
  component: ListItem,
};

const Template = (args) => <ListItem {...args} />;
const TextTemplate = (args) => <ListItem {...args} />;
const TextButtonTemplate = (args) => <ListItem {...args} />;
const ArrowTextButtonTemplate = (args) => <ListItem {...args} />;
const ArrowTemplate = (args) => <ListItem {...args} />;
const SwitchTemplate = (args) => <ListItem {...args} />;
const TickTemplate = (args) => <ListItem {...args} />;
const CheckboxTemplate = (args) => <ListItem {...args} />;
const RadioTemplate = (args) => <ListItem {...args} />;

export const Default = Template.bind({});
export const TextListItem = TextTemplate.bind({});
export const TextButtonListItem = TextButtonTemplate.bind({});
export const ArrowTextButtonListItem = ArrowTextButtonTemplate.bind({});
export const ArrowListItem = ArrowTemplate.bind({});
export const SwitchListItem = SwitchTemplate.bind({});
export const TickListItem = TickTemplate.bind({});
export const CheckboxListItem = CheckboxTemplate.bind({});
export const RadioListItem = RadioTemplate.bind({});

Default.args = {
  Icon: Phone,
  leftLabel: "Left Label",
  type: null,
  hideUnderline: true,
};

TextListItem.args = {
  Icon: Phone,
  leftLabel: "Left Label",
  rightLabel: "Right Label",
  type: 'text',
  leftBold: true,
};

TextButtonListItem.args = {
  Icon: Phone,
  leftLabel: "Left Label",
  rightLabel: "Right Button Label",
  type: 'textBtn',
  leftBold: true,
  body: 'Este es el cuerpo',
  onClick: action('onClick')
}

ArrowTextButtonListItem.args = {
  Icon: Phone,
  leftLabel: "Left Label",
  rightLabel: "Right Label",
  type: 'arrowTextBtn',
  onClick: action('onClick')
}

ArrowListItem.args = {
  Icon: Phone,
  leftLabel: "Left Label",
  type: 'arrow',
  leftBold: true,
  body: 'Este es el cuerpo',
  onClick: action('onClick')
}

SwitchListItem.args = {
  Icon: Phone,
  leftLabel: "Left Label",
  type: 'switch',
  onClick: action('onClick')
}

TickListItem.args = {
  Icon: Phone,
  leftLabel: "Left Label",
  type: 'tick',
  leftBold: true,
  body: 'Este es el cuerpo',
}

CheckboxListItem.args = {
  leftLabel: "Left Label",
  type: 'checkbox',
  value: false,
  onClick: action('onClick')
}

RadioListItem.args = {
  leftLabel: "Left Label",
  type: 'radio',
  value: false,
  onClick: action('onClick')
}


