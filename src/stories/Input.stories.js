import {Input}  from '../components/Input/Input';

export default {
  title: 'Alterna/Input',
  component: Input,
};

const Template = (args) => <Input {...args} />;

export const Base = Template.bind({});
Base.args = {
  required: false,
  placeholder: '',
  value: '',
  label: 'prueba',
  type: '',
  width: '100px',
  disabled: false,
  error: false,
  errorText: '',
};