import { BtnDefault } from "../components/Button/BtnDefault";
import { BtnIcons } from "../components/Button/BtnIcons";
import { BtnText } from "../components/Button/BtnText";
import { BtnModal } from "../components/Button/BtnModal";
import ArrowLeft from "../assets/images/svg/ArrowIconL";
import Phone from "../assets/images/svg/Phone";
import ArrowRight from "../assets/images/svg/ArrowIconR";
import { action } from '@storybook/addon-actions';

export default {
  title: "Alterna/Button",
  component: BtnDefault,
  subcomponents: { BtnIcons, BtnText, BtnModal },
  decorators: [(Story) => (
    <div style={{width: "300px"}}>
      <Story />
    </div>
  )]
};

const Template = (args) => <BtnDefault {...args} />;
const ButtonIconsTemplate = (args) => <BtnIcons {...args} />;
const ButtonTextTemplate = (args) => <BtnText {...args} />;
const ButtonModalTemplate = (args) => <BtnModal {...args} />;

export const Default = Template.bind({});
Default.args = {
  colorSecondary: "#D6D6D6",
  colorDefault: "#000000",
  IconLeft: ArrowLeft,
  IconRight: ArrowRight,
  disabled: false,
  font: "hButtonSemibold",
  loading: false,
  success: false,
  text: "guardar cambios",
  mobile:false,
  color:"red",
  onPress: action('onClick')
};

export const ButtonIcons = ButtonIconsTemplate.bind({});
ButtonIcons.args = {
  Icon: ArrowLeft,
  mobile: false,
  onPress: action('onClick')
};

export const ButtonText = ButtonTextTemplate.bind({});
ButtonText.args = {
  mobile: false,
  title: "Title",
  subtitle: "Subtitle",
  onPress: action('onClick'),
  Icon: Phone,
  fontTilte: "body1Semibold",
  fontSubtitle: "body1Regular",
};

export const ButtonModal = ButtonModalTemplate.bind({});
ButtonModal.args = {
  onPress: action('onClick'),
  disabled: false,
  text: "Texto botón",
  mobile: false,
  IconLeft: ArrowLeft,
  IconRight: ArrowRight,
  font: "hButtonSemibold",
};
