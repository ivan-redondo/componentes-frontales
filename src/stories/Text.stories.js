import {Text}  from '../components/Text/Text';

export default {
  title: 'Alterna/Text',
  component: Text,
};

const Template = (args) => <Text {...args}>prueba</Text>;

export const Base = Template.bind({});
Base.args = {
  padBot: 0,
  mLeft: 0,
  mRight: 0,
  cursor: 'default',
  opacity: 1,
  color: 'black',
  mobile: false,
  theme: 'h1Semibold',
};