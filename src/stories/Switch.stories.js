
import { action } from '@storybook/addon-actions';
import { SwitchItem } from '../components/Switch/SwitchItem';

export default {
  title: "Alterna/Switch",
  component: SwitchItem,
};

const Template = (args) => <SwitchItem {...args} />;

export const Default = Template.bind({});
Default.args = {
  value: false,
  onClick: action('onClick')
};
