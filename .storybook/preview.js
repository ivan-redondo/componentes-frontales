import { StyleInjector } from '../src/components/StyleInjector/StyleInjector';

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
}

export const decorators = [
  (Story) => (
    <StyleInjector>
      <Story />
    </StyleInjector>
  ),
];
